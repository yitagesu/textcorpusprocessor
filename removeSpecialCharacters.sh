#!/bin/bash          

#remove commas
sed 's/,//g' $1 > temp.txt

#remove dots
sed -e 's/\.//g' temp.txt > temp1.txt

#remove hiphens from words list
sed 's/-/ /g' temp1.txt > temp.txt

#remove dollar signs from words list
sed 's/\$//g' temp.txt > temp1.txt

#remove exclamation marks from words list
sed 's/!//g' temp1.txt > temp.txt

#remove question marks from words list
sed 's/\?//g' temp.txt > temp1.txt

#remove quotation marks from words list
sed 's/"//g' temp1.txt > temp.txt
sed 's/“//g' temp.txt > temp1.txt
sed 's/”//g' temp1.txt > temp.txt

#remove parentheses from words list
sed 's/(//g' temp.txt > temp1.txt
sed 's/)//g' temp1.txt > temp.txt

#remove columns from words list
sed 's/://g' temp.txt > temp1.txt

#remove semi columns from words list
sed 's/;//g' temp1.txt > temp.txt

#remove asterisks from words list
sed 's/*//g' temp.txt > temp1.txt

#remove ampersands from words list
sed 's/&/ /g' temp1.txt > temp.txt

#remove / from words list
sed 's/\// /g' temp.txt > temp1.txt

#expand the percentage (%) symbol
sed 's/%/ percent/g' temp1.txt > temp.txt

#expand the at (@) symbol
sed 's/@/ at/g' temp.txt > temp1.txt

#expand the plus (+) symbol
sed 's/+/ plus/g' temp1.txt > temp.txt

#expand the assignment (=) symbol
sed 's/=/ is equal to /g' temp.txt > temp1.txt

#expand the less than (<) symbol
sed 's/</ less than /g' temp1.txt > temp.txt

#expand the greater than (>) symbol
sed 's/>/ greater than /g' temp.txt > $1
#delete temporary files
rm temp.txt
rm temp1.txt
