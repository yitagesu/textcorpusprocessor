#include <string>
#include <sstream>
#include <fstream>
#include <iostream>

using namespace std;

void textNormalizer(string textCorpus) {

    string cmd = "export PATH=~/Chickasaw:$PATH";
    system(cmd.c_str());

    //remove special charcters from the text corpus
    cmd = "removeSpecialCharacters.sh " + textCorpus;
    system(cmd.c_str());
}

//convert a text corpus into a list of words
void createWordList(string textCorpus) {

    //convert text corpus into list of words
    string cmd = "cat " + textCorpus + " | tr ' ' '\n' > temp.txt";
    system(cmd.c_str()); 

    //convert every word in words list to lowercase
    system("dd if=temp.txt of=word-list.txt conv=lcase");
    system("rm temp.txt");
}

//converts a word into its IPA pronunciation
void getIPATranscription(string word, string dictionary) {

    string line;
    ifstream infile( dictionary.c_str() );

    if (infile) {
        while (getline( infile, line ))  {         
	    istringstream iss(line);
      	    string sub;
            iss >> sub;

    	    if(sub == word) {
		ofstream outfile("IPA-Transcription.txt", std::ios_base::app);
    	    	cout << line.substr( sub.length() ) << endl;
		outfile << line.substr( sub.length() ) << endl;
		outfile.close();
		break;
	    }
        }
    }

    infile.close();
}

//converts words in a text corpus into their IPA pronunciation
void convertWordListToIPAForm(string fileName) {

    string word;
    ifstream infile( fileName.c_str() );
    if (infile) {

        while (getline( infile, word ))  {
            getIPATranscription( word, "en-US-lex.txt" );
        }
    }
    infile.close();
}

//counts the phonemes in a  text corpus
void countPhonemes(string phonemeList, string IPATextCorpus) {

    ofstream outfile("phoneme-stats.txt");
    outfile << "Phoneme     	   Count\n";
    outfile << "=======            =====\n";
    outfile.close();
    
    string phoneme;
    ifstream infile( phonemeList.c_str() );
    if (infile) {

	while (getline( infile, phoneme ))  {

	    ofstream outfile("phoneme-stats.txt", std::ios_base::app);
 	    string tmp = phoneme + "		";
	    outfile << tmp;
	    outfile.close();

	    string cmd = "grep -o \'\\b" + phoneme + "\\b\' " + IPATextCorpus + " | wc -w >> phoneme-stats.txt";
      	    system(cmd.c_str());
	}
    }
    infile.close();
    outfile.close();
}

int main() {

    //textNormalizer("text-corpus.txt");

    createWordList("text-corpus.txt"); 
    convertWordListToIPAForm("word-list.txt"); 
    countPhonemes("en-US-phones.txt", "IPA-Transcription.txt");

    return 0;
}



